# Decisionmakr

> Let's decide your next action!

Not sure what to do next? Type in your options and make a decision!

## Website

[https://decisionmakr.woidbua.com/](https://decisionmakr.woidbua.com/)

## Demo

![Demo](demo.gif)
